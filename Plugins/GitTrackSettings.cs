using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace DigitalPenguin.Settings
{
    [Serializable]
    public class GitTrackSettings : ScriptableObject
    {
        private static bool instanced = false;
        private static GitTrackSettings instance;
        
        public static GitTrackSettings Instance
        {
            get {
                if (!instanced)
                {
                    Load();
                }

                return instance;
            }
        }

        private static void Load()
        {
            instanced = true;
            
            instance = Resources.Load<GitTrackSettings>("GitTrackSettings");
            if(instance == null)
                instance = CreateInstance<GitTrackSettings>();
        }
        
        [Header("Update Behaviour")]
        [Tooltip("Reads the information about git after a script compile.")]
        public bool updateOnScriptCompile = false;
        [Tooltip("Reads the information about git when you hit play in the editor.")]
        public bool updateOnPlayInEditor = true;
        [Tooltip("Reads the information about git during the build process.")]
        public bool updateOnPreBuild = true;
        [Tooltip("If selected all commits are counted. If not selected only commits based on HEAD are counted.")]
        public bool countAllCommits = false;
        
        [Header("Build Behaviour")]
        public bool parseVersionOnPlay = true;
        public bool overwriteBundleVersion = true;
        public bool overwriteSwitchDisplayVersion = true;
        public bool overwriteSwitchReleaseVersion = true;
        public bool overwriteXBoxOneReleaseVersion = true;
        public bool overwritePS4AppVersion = true;
        public bool overwritePs4MasterVersion = true;
        public bool overwriteMacOSBuildNumber = true;
        public bool appBundleIsCommitCount = true;
        public bool logVersionOnPlayMode = true;
        public bool pathIsAbsolute = false;
        public string repositoryPath = "";

        [Header("Version Format")][TextArea(2,2)]
        public string shortVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string longVersionFormat = "$TagCount.$BranchCount.$CommitCount \"$Tag\" $DateShort\nBuilt on $BuildMachine $BuildDateLong";

        [TextArea(2,2)]
        public string bundleVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string switchReleaseVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string switchDisplayVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string xboxOneVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string PS4AppVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [TextArea(2,2)]
        public string Ps4MasterVersionFormat = "$TagCount.$BranchCount.$CommitCount";

        [Header("Search Patterns")]
        [Tooltip("Match the pattern to all branch names and only select branches that match.")]
        public string branchPattern = "";
        [Tooltip("Match the pattern to all tag names and only select tags that match.")]
        public string tagPattern = "";
        [Tooltip("The number of commits to be saved to file. Warning: One commit is always needed to get the current commit info.")]
        public int maxCommitCount = 16;

#if UNITY_EDITOR
        [MenuItem("Tools/Git Track/Check Or Create Settings")]
        public static void CheckOrCreateFile()
        {
            if (Resources.Load<GitTrackSettings>("GitTrackSettings") == null)
            {
                // file does not exist.
                var newInstance = CreateInstance<GitTrackSettings>();
                AssetDatabase.CreateAsset(newInstance, "Assets/Resources/GitTrackSettings.asset");
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }

        [CustomEditor(typeof(GitTrackSettings))]
        private class GitTrackSettingsEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                
                GitTrackSettings instance = target as GitTrackSettings;
                
                
                if (GUILayout.Button("Save & Reload"))
                {
                    GitTrackSettings.instance = instance;
                    GitTrackExtentions.UpdateGitTrack();
                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    Load();
                    GitTrack.Reload();
                }
                
                GUILayout.Label(GitTrack.Parse($"Short Version: {instance.shortVersionFormat}"));
                GUILayout.Label(GitTrack.Parse($"Long Version: {instance.longVersionFormat}"));
            }
        }
#endif
    }
}