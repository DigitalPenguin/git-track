﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using DigitalPenguin.GitTrackUtility;
using DigitalPenguin.Settings;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace DigitalPenguin
{
    public static class GitTrackExtentions
    {
        /// <summary>
        /// Executes a given Git command.
        /// Source from: https://stewmcc.com/post/git-commands-from-unity/
        /// </summary>
        /// <param name="gitCommand"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static string RunGitCommand(string gitCommand, string workingDirectory = "")
        {
            // Strings that will catch the output from our process.
            string output = "no-git";
            string errorOutput = "no-git";

            // Set up our processInfo to run the git command and log to output and errorOutput.
            ProcessStartInfo processInfo = new ProcessStartInfo("git", @gitCommand)
            {
                CreateNoWindow = true, // We want no visible pop-ups
                UseShellExecute = false, // Allows us to redirect input, output and error streams
                RedirectStandardOutput = true, // Allows us to read the output stream
                RedirectStandardError = true, // Allows us to read the error stream
            };

            if (workingDirectory != "")
            {
                processInfo.WorkingDirectory = workingDirectory;
            }

            // Set up the Process
            Process process = new Process
            {
                StartInfo = processInfo
            };

            try
            {
                process.Start(); // Try to start it, catching any exceptions if it fails
            }
            catch (Exception e)
            {
                // For now just assume its failed cause it can't find git.
                Debug.LogError("Git is not set-up correctly, required to be on PATH, and to be a git project.");
                throw e;
            }

            // Read the results back from the process so we can get the output and check for errors
            output = process.StandardOutput.ReadToEnd();
            errorOutput = process.StandardError.ReadToEnd();

            process.WaitForExit(); // Make sure we wait till the process has fully finished.
            process.Close(); // Close the process ensuring it frees it resources.

            // Check for failure due to no git setup in the project itself or other fatal errors from git.
            if (output.Contains("fatal") || output == "no-git")
            {
                Debug.LogError("Command: git " + @gitCommand + " Failed\n" + output + errorOutput);
            }

            // Log any errors.
            if (errorOutput != "")
            {
                Debug.LogError("Git Error: " + errorOutput);
            }

            return output; // Return the output from git.
        }

        #region MenuItems 

        [MenuItem("Tools/Git Track/Print Version")]
        public static void PrintVersion()
        {
            Debug.Log(GitTrack.Version);
        }
        
        [MenuItem("Tools/Git Track/Refresh Version")]
        public static void UpdateGitTrack()
        {
            // check for the settings file
#if UNITY_EDITOR
            if (!Directory.Exists($"{Application.dataPath}/Resources"))
            {
                Directory.CreateDirectory($"{Application.dataPath}/Resources");
            }

            GitTrackSettings.CheckOrCreateFile();
#endif
            
            GitTrack gitTrack = new GitTrack(true);
            
            gitTrack.Init();
            
#if UNITY_EDITOR
            string text = JsonUtility.ToJson(gitTrack, true);
            if (!File.Exists($"{Application.dataPath}/Resources/GitTrackVersion.txt"))
            {
                File.WriteAllText($"{Application.dataPath}/Resources/GitTrackVersion.txt", text);
                AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            }
            else
            {
                File.WriteAllText($"{Application.dataPath}/Resources/GitTrackVersion.txt", text);
            }

            Debug.Log($"Git Track Version: {gitTrack}");
#endif
        }

        #endregion


        public static GitTrackCommit[] GetCommits(int count = -1)
        {
            // gets a log of all commits and then parses them to an array of commits
            List<GitTrackCommit> allCommits = new List<GitTrackCommit>(count);

            string[] command = RunGitCommand("log", GetGitPath())
                .Split('\n');

            GitTrackCommit commit = null;
            for (int i = 0; i < command.Length; i++)
            {
                string line = command[i];
                if (line.StartsWith("commit "))
                {
                    // add last element to the list
                    if (commit != null)
                        allCommits.Add(commit);

                    if (allCommits.Count == count)
                        break;

                    commit = new GitTrackCommit();

                    ParseFirstLine(ref commit, line);
                }
                else if (line.StartsWith("Author:"))
                {
                    ParseAuthorLine(ref commit, line);
                }
                else if (line.StartsWith("Date:"))
                {
                    ParseDateLine(ref commit, line);
                }
                else
                {
                    // search for a line with actual text in it.
                    if (line.StartsWith("    ") && line.Trim() != String.Empty)
                    {
                        string l = new string('\n', 0);
                        if (commit.description == "")
                            commit.description = line.Trim();
                        else
                            commit.description = $"{commit.description}{l}{line.Trim()}";
                    }
                }
            }

            if (commit != null && allCommits.Count != count)
                allCommits.Add(commit);


            return allCommits.ToArray();
        }

        private static void ParseFirstLine(ref GitTrackCommit element, string line)
        {
            //commit e4da0737fadbe3f4ed068a16b63682f13bae221b (HEAD -> feature/git-track-info, origin/feature/git-track-info)
            // remove the 'commit text' at the beginning
            line = line.Substring(7, line.Length - 7);

            int index = line.IndexOf(' ');
            index = index > 0 ? index : line.Length;

            string longHash = line.Substring(0, index);

            string shortHash = longHash.Substring(0, 7);
            element.longHash = longHash;
            element.shortHash = shortHash;
        }

        private static void ParseAuthorLine(ref GitTrackCommit element, string line)
        {
            string[] auth = line.Replace("Author:", "").Trim().Split('<');
            element.authorName = auth[0].Trim();
            if (auth.Length > 1)
            {
                element.authorMail = auth[1].Replace(">", "").Trim();
            }
        }

        private static void ParseDateLine(ref GitTrackCommit element, string line)
        {
            string text = line.Replace("Date:", "").Trim();

            DateTimeOffset dto = new DateTimeOffset();
            DateTimeOffset.TryParseExact(text, "ddd MMM d HH:mm:ss yyyy K",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out dto);

            element.date = dto.ToUniversalTime().ToFileTime();
        }

        public static int GetCommitCount()
        {
            string log = RunGitCommand(
                "rev-list " + (GitTrackSettings.Instance.countAllCommits ? "--all" : "HEAD") + " --count",GetGitPath());
            return int.Parse(log.Trim());
        }

        public static string[] GetBranches(out string currentBranch)
        {
            string pattern = GitTrackSettings.Instance.branchPattern;
            
            bool containsPattern = pattern != string.Empty && ((!pattern.StartsWith("*") && !pattern.EndsWith("*")) || (pattern.StartsWith("*") && pattern.EndsWith("*")));
            bool startsWithPattern = pattern != string.Empty && pattern.EndsWith("*") && !pattern.StartsWith("*");
            bool endsWithPattern = pattern != string.Empty && pattern.StartsWith("*") && !pattern.EndsWith("*");
            StringBuilder builder = new StringBuilder(pattern);
            if(pattern.StartsWith("*"))
                builder.Remove(0, 1);
            if (pattern.EndsWith("*") && builder.Length != 0)
                builder.Remove(builder.Length - 1, 1);
            pattern = builder.ToString();
            if (pattern == string.Empty)
            {
                containsPattern = startsWithPattern = endsWithPattern = false;
            }
            
            string log = RunGitCommand("branch -a", GetGitPath());
            List<string> vals = new List<string>(log.Split('\n'));
            

            currentBranch = "";

            for (int i = 0; i < vals.Count; i++)
            {
                if (vals[i].StartsWith("* "))
                {
                    currentBranch = vals[i].Replace("*", "").Trim();
                }

                if (!vals[i].Contains("remotes/") || vals[i].Contains("HEAD -> "))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }
                
                builder.Clear();
                builder.Append(vals[i]);
                builder.Replace("remotes/origin/", "");
                builder.Replace("remotes/", "");
                vals[i] = builder.ToString().Trim();

                if (containsPattern && !vals[i].Contains(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }

                if (startsWithPattern && !vals[i].StartsWith(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }

                if (endsWithPattern && !vals[i].EndsWith(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            return vals.ToArray();
        }

        public static string[] GetTags()
        {
            string pattern = GitTrackSettings.Instance.tagPattern;
            
            bool containsPattern = pattern != string.Empty && ((!pattern.StartsWith("*") && !pattern.EndsWith("*")) || (pattern.StartsWith("*") && pattern.EndsWith("*")));
            bool startsWithPattern = pattern != string.Empty && pattern.EndsWith("*") && !pattern.StartsWith("*");
            bool endsWithPattern = pattern != string.Empty && pattern.StartsWith("*") && !pattern.EndsWith("*");
            StringBuilder builder = new StringBuilder(pattern);
            if(pattern.StartsWith("*"))
                builder.Remove(0, 1);
            if (pattern.EndsWith("*"))
                builder.Remove(builder.Length - 1, 1);
            pattern = builder.ToString();
            if (pattern == string.Empty)
            {
                containsPattern = startsWithPattern = endsWithPattern = false;
            }
            
            List<string> vals =
                new List<string>(RunGitCommand("tag", GetGitPath())
                    .Split('\n'));
            for (int i = 0; i < vals.Count; i++)
            {
                if (vals[i].Trim() == "")
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }

                if (containsPattern && !vals[i].Contains(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }
                
                if (startsWithPattern && !vals[i].StartsWith(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }
                
                if (endsWithPattern && !vals[i].EndsWith(pattern))
                {
                    vals.RemoveAt(i);
                    i--;
                    continue;
                }

            }

            return vals.ToArray();
        }

        private static string GetGitPath()
        {
            if (GitTrackSettings.Instance.pathIsAbsolute)
            {
                return GitTrackSettings.Instance.repositoryPath;
            }
            else
            {
                return $"{Application.dataPath}/{GitTrackSettings.Instance.repositoryPath}";
            }
        }

        
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void PlayRunStandalone()
        {
            
            #if UNITY_EDITOR
            if (GitTrackSettings.Instance.updateOnPlayInEditor && !GitTrackSettings.Instance.updateOnScriptCompile)
            {
                GitTrackExtentions.UpdateGitTrack();
            }
            #endif
            
            if (GitTrackSettings.Instance.parseVersionOnPlay)
            {
                GitTrack.ParseVersions();
            }
                
            if (GitTrackSettings.Instance.logVersionOnPlayMode)
            {
                Debug.Log($"Version: {GitTrack.Version}");
            }
        }
        
    }
}
#endif