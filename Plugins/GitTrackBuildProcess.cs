#if UNITY_EDITOR

using DigitalPenguin;
using DigitalPenguin.Settings;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace ThirdParty.GitTrack.Plugins
{
    public class GitTrackBuildProcess : IPreprocessBuildWithReport
    {
        private IPreprocessBuildWithReport preprocessBuildWithReportImplementation;

        public int callbackOrder => 0;
        
        public void OnPreprocessBuild(BuildReport report)
        {
            if (GitTrackSettings.Instance.updateOnPreBuild)
            {
                GitTrackExtentions.UpdateGitTrack();
            }
        }
        
        [InitializeOnLoad]
        private class OnPlay
        {
            static OnPlay()
            {
                if (GitTrackSettings.Instance.updateOnScriptCompile)
                {
                    Debug.Log("Updating on compile");
                    GitTrackExtentions.UpdateGitTrack();
                }
            }
        }
    }
}
#endif