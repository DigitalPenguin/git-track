using System;

namespace DigitalPenguin.GitTrackUtility
{
    [Serializable]
    public class GitTrackCommit
    {
        public string shortHash;
        public string longHash;
        public string authorName;
        public string authorMail;
        public long date;
        public string description;
    }
}