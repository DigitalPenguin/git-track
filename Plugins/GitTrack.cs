﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using DigitalPenguin.GitTrackUtility;
using DigitalPenguin.Settings;
using UnityEngine;

namespace DigitalPenguin
{
    [Serializable]
    public class GitTrack
    {
        #region initialization
        private static bool instanced = false;
        private static GitTrack instance = null;
        private static GitTrack Instance
        {
            get {
                if (!instanced)
                {
                    instance = new GitTrack();
                    instanced = true;
                }

                return instance;
            }
        }
        
        public GitTrack(bool createEmpty = false)
        {
            if (createEmpty)
                return;
            
            // Look for git track file in the project
            TextAsset gitTrackAsset = Resources.Load<TextAsset>("GitTrackVersion");
            if (gitTrackAsset != null)
            {
                JsonUtility.FromJsonOverwrite(gitTrackAsset.text, this);
            }

            buildDate = DateTime.FromFileTimeUtc(createdAt);

        }

        public static void Reload()
        {
            instance = new GitTrack();
        }

        [Conditional("UNITY_EDITOR")]
        internal void Init()
        {
            buildDate = DateTime.Now;
            createdAt = buildDate.ToFileTimeUtc();

            createdByUser = Environment.UserName;
            createdByMachine = Environment.MachineName;
            createdOnOS = Environment.OSVersion.ToString();

            commits = GitTrackExtentions.GetCommits(GitTrackSettings.Instance.maxCommitCount);

            commitCount = GitTrackExtentions.GetCommitCount();

            tags = GitTrackExtentions.GetTags();
            
            branches = GitTrackExtentions.GetBranches(out currentBranch);
            
            branchCount = branches.Length;
            tagCount = tags.Length;

            instance = this;
            
            version = ParseString(GitTrackSettings.Instance.shortVersionFormat);
            longVersion = ParseString(GitTrackSettings.Instance.longVersionFormat);

            if (GitTrackSettings.Instance.overwriteBundleVersion)
            {
                UnityEditor.PlayerSettings.bundleVersion = ParseString(GitTrackSettings.Instance.bundleVersionFormat);
            }
            if (GitTrackSettings.Instance.overwriteSwitchDisplayVersion)
            {
                UnityEditor.PlayerSettings.Switch.displayVersion = ParseString(GitTrackSettings.Instance.switchDisplayVersionFormat);
            }
            if (GitTrackSettings.Instance.overwriteSwitchReleaseVersion)
            {
                UnityEditor.PlayerSettings.Switch.releaseVersion = ParseString(GitTrackSettings.Instance.switchReleaseVersionFormat);
            }
            if (GitTrackSettings.Instance.overwriteXBoxOneReleaseVersion)
            {
                UnityEditor.PlayerSettings.XboxOne.Version = ParseString(GitTrackSettings.Instance.xboxOneVersionFormat);
            }
            if (GitTrackSettings.Instance.overwritePS4AppVersion)
            {
                UnityEditor.PlayerSettings.PS4.appVersion = ParseString(GitTrackSettings.Instance.PS4AppVersionFormat);
            }
            if (GitTrackSettings.Instance.overwritePs4MasterVersion)
            {
                UnityEditor.PlayerSettings.PS4.masterVersion = ParseString(GitTrackSettings.Instance.Ps4MasterVersionFormat);
            }
            if (GitTrackSettings.Instance.overwriteMacOSBuildNumber)
            {
                UnityEditor.PlayerSettings.macOS.buildNumber = commitCount.ToString();
            }
            if (GitTrackSettings.Instance.appBundleIsCommitCount)
            {
                UnityEditor.PlayerSettings.Android.bundleVersionCode = commitCount;
                UnityEditor.PlayerSettings.iOS.buildNumber = commitCount.ToString();
            }
        }

        private string ParseString(string input)
        {
            StringBuilder builder = new StringBuilder(input);
            builder.Replace("$TagCount", tagCount.ToString(CultureInfo.CurrentCulture));
            builder.Replace("$BranchCount", branchCount.ToString(CultureInfo.CurrentCulture));
            builder.Replace("$CommitCount", commitCount.ToString(CultureInfo.CurrentCulture));
            
            builder.Replace("$Tag", Tag);
            builder.Replace("$Commit", Commit);
            builder.Replace("$Branch", Branch);
            builder.Replace("$Author", LastCommit.authorName);
            
            builder.Replace("$DateShort", DateTime.FromFileTimeUtc(LastCommit.date).ToString("d", CultureInfo.CurrentCulture));
            builder.Replace("$DateLong", DateTime.FromFileTimeUtc(LastCommit.date).ToString("D", CultureInfo.CurrentCulture));
            
            builder.Replace("$BuildDateShort", buildDate.ToString("d", CultureInfo.CurrentCulture));
            builder.Replace("$BuildDateLong", buildDate.ToString("D", CultureInfo.CurrentCulture));

            builder.Replace("$BuildUser", createdByUser);
            builder.Replace("$BuildMachine", createdByMachine);
            builder.Replace("$BuildOS", createdOnOS);
            
            builder.Replace("$CurrentUser", Environment.UserName);
            builder.Replace("$CurrentMachine", Environment.MachineName);
            builder.Replace("$CurrentOS", Environment.OSVersion.ToString());
            
            builder.Replace("$ShortVersion", version);
            builder.Replace("$LongVersion", longVersion);
            
            builder.Replace("$Description", LastCommit.description);
            builder.Replace("$AuthorMail", LastCommit.authorMail);

            return builder.ToString();
        }

        public static string Parse(string input)
        {
            return Instance.ParseString(input);
        }

        
        #endregion initialization
    
        #region public getters

        public static string Commit => LastCommit.shortHash;

        public static string Version => Instance.version;
        public static string LongVersion => Instance.longVersion;
        

        public static GitTrackCommit LastCommit => GetCommit(0);
        public static string Tag => GetTag(0);
        public static string Branch => Instance.currentBranch;

        #endregion public getters
    
        #region private variables

        [SerializeField] private int commitCount = 0;
        [SerializeField] private int branchCount = 0;
        [SerializeField] private int tagCount = 0;
        [SerializeField] private string createdByUser = "";
        [SerializeField] private string createdByMachine = "";
        [SerializeField] private string createdOnOS = "";
        [SerializeField] private long createdAt = 0;
        [SerializeField] private string currentBranch = "";
        [SerializeField] private string version = "";
        [SerializeField] private string longVersion = "";
        
        [SerializeField] private string[] tags, branches;

        [SerializeField] private GitTrackCommit[] commits = new GitTrackCommit[0];

        [NonSerialized] private DateTime buildDate;

        #endregion private variables
        
        #region public methods

        public static GitTrackCommit GetCommit(int positionInHierarchy)
        {
            if (Instance.commits.Length > positionInHierarchy)
            {
                return Instance.commits[positionInHierarchy];
            }

            return new GitTrackCommit();
        }
        
        public static string GetTag(int positionInHierarchy)
        {
            if (Instance.tags.Length > positionInHierarchy)
            {
                return Instance.tags[positionInHierarchy];
            }

            return "no tag";
        }
        
        #endregion
        
        #region Utility

        public override string ToString()
        {
            return $"{version} - {buildDate:f} by {createdByUser} on {createdByMachine}";
        }

        #endregion

        public static void ParseVersions()
        {
            Instance.version = Instance.ParseString(GitTrackSettings.Instance.shortVersionFormat);
            Instance.longVersion = Instance.ParseString(GitTrackSettings.Instance.longVersionFormat);
 
        }
    }
}