﻿using System;
using DigitalPenguin;
using UnityEngine;
using UnityEngine.Events;

[ExecuteInEditMode]
public class GitVersionDisplay : MonoBehaviour
{
    private enum VersionType
    {
        ShortVerion, LongVersion, CustomVersion
    }

    [SerializeField] private VersionType versionType = VersionType.ShortVerion;
    [TextArea][SerializeField] private string customVersionText = "";
    [SerializeField] private UnityGitVersionEvent OnSetText = new UnityGitVersionEvent();
    
    [Serializable]
    public class UnityGitVersionEvent : UnityEvent<string>{}
    
    void OnEnable()
    {
        ParseVersion();
    }

    #if UNITY_EDITOR
    void Update()
    {
        if (Application.isPlaying)
            return;
        ParseVersion();
    }
    #endif

    private void ParseVersion()
    {
        string versionText = "no version";
        switch (versionType)
        {
            case VersionType.LongVersion:
                versionText = GitTrack.LongVersion;
                break;
            case VersionType.CustomVersion:
                versionText = GitTrack.Parse(customVersionText);
                break;
            case VersionType.ShortVerion:
            default:
                versionText = GitTrack.Version;
                break;
        }

        OnSetText.Invoke(versionText);
    }
}
