# Git Track

Git Track is a very small plugin for Unity to retrieve the current version of your app or game during the build process, while pressing play or on script compile.

## Install

Download a release and put in your Third Party folder in your unity project.
Now navigate to `Tools > Git Track > Refresh Version`.

This should create a version file and a settings file. You can move the settings file anywhere you want as long as it is inside a Resources folder.

To modify to folder of the info file, please setup the path in the settings file. Then click Save and Update.

## Settings file

The settings file lets you modify how Git Tracks works.

### Usable variables

You can specify how the version strings will be formatted by using text and the following variables.

*Warning! Some of the variables contain personal and sensitive data. Do not use the variables during production unlees, you have notified the users. Sensitive variables are marked with a `*`*


|Variables | Description | Example  
|---	|---    |---
|$TagCount      | The number of tags        | 3
|$BranchCount   | The number of branches        | 8
|$CommitCount   | The number of commits| 345
|$Tag           | The most recent tag name        | Latest-Release
|$Commit        | *(Sensitive)* The current abbreviated commit hash        | be23dd1
|$CommitLong        | The current commit hash        |  be23dd1c237ee2a9ee9f5accf371e2d1d3603842
|$Branch        | The current branch name        | develop
|$Author*        | *(Sensitive)* The author of the current commit        | Max Mustermann
|$DateShort     | A short version of the date of the commit (d)        | 04/10/2008
|$DateLong      | A longer version of the date of the commit (D)        | Thursday, 10 April 2008
|$BuildDateShort     | A short version of the date of the build        | 04/10/2008
|$BuildDateLong      | A longer version of the date of the commit        | Thursday, 10 April 2008
|$BuildUser*     | *(Sensitive)* The OS Username during the build process        | SomeUser
|$BuildMachine*  | *(Sensitive)* The OS machine name during the build process        | N/A
|$BuildOS*       | *(Sensitive)* The OS version during the build process        | N/A
|$CurrentUser*   | *(Sensitive)* The current OS Username        | SomeUser
|$CurrentMachine*   | *(Sensitive)* The current OS machine        | N/A
|$CurrentOS*     | *(Sensitive)* The current OS version        | N/A
|$ShortVersion|The short version string (Beware nesting!)| 1.0.23
|$LongVersion|The long version string (Beware nesting!)| 1.2 - Build 34 'Some Release Tag' on branchname
|$Description|The last commit message.|Added a new feature and fixed a bug.
|$AuthorMail|The mail of the author|mail@example.com